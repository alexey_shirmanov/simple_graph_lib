**Simple Graph library**

Build requirements:

  
* Java 8
* maven 3.3 and above

Build command:

      mvn clean install
      
Examples of usage:

```
//create an undirected graph with string vertices

  Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
  String v1 = "one";
  String v2 = "two";
  String v3 = "three";
    
//add vertices to the graph
  stringGraph.addVertex(v1)
  stringGraph.addVertex(v2)
  stringGraph.addVertex(v3)
  
// add edges between vertices
  stringGraph.addEdge(v1, v2)
  stringGraph.addEdge(v2, v3)
  
//get path between vertices
 final List<Edge<String>> path  = stringGraph.getPath(v1, v3);//contains edges {v1, v2}, {v2, v3}
 final List<Edge<String>> pathBack  = stringGraph.getPath(v3, v1);//contains edges {v3, v2}, {v2, v1}

//create a directed graph with string vertices
  Graph<String, Edge<String>> stringGraph = new DirectedGraph<>();
  String v1 = "one";
  String v2 = "two";
  String v3 = "three";
  stringGraph.addVertex(v1)
  stringGraph.addVertex(v2)
  stringGraph.addVertex(v3)
  stringGraph.addEdge(v1, v2)
  stringGraph.addEdge(v2, v3)
  
  //get path for directed graph
   final List<Edge<String>> path  = stringGraph.getPath(v1, v3);//contains edges {v1, v2}, {v2, v3}
   final List<Edge<String>> pathBack  = stringGraph.getPath(v3, v1);//empty because edges are not bi-directional
 
```
    