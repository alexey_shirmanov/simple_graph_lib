package com.natera.simplegraph;

public class GraphException extends RuntimeException {
    public GraphException() {
    }

    public GraphException(final String message) {
        super(message);
    }

    public GraphException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GraphException(final Throwable cause) {
        super(cause);
    }

    public GraphException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
