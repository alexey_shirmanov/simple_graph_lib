package com.natera.simplegraph;

public class DirectedGraph<V, E extends Edge<V>> extends AbstractGraph<V, Edge<V>> {
    @Override
    protected Edge<V> getEdge(final V from, final V to) {
        return new SimpleEdge<>(from, to);
    }
}
