package com.natera.simplegraph;

import java.util.Objects;

public class SimpleEdge<V> implements Edge<V> {
    private V source;
    private V destination;

    public SimpleEdge(final V source, final V destination) {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public V getSourceVertex() {
        return source;
    }

    @Override
    public V getDestinationVertex() {
        return destination;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final SimpleEdge<?> that = (SimpleEdge<?>) o;
        return Objects.equals(source, that.source) &&
                Objects.equals(destination, that.destination);
    }

    @Override
    public String toString() {
        return "SimpleEdge{" +
                "source=" + source +
                ", destination=" + destination +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, destination);
    }
}
