package com.natera.simplegraph;

import java.util.*;
import java.util.function.Function;

import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.EMPTY_SET;
import static java.util.Objects.isNull;
import static java.util.Optional.empty;
import static java.util.stream.Collectors.toSet;

/**
 * abstract class for both of directed and undirected graph. Implementation uses a data structure similar to Adjacency list.
 * graph vertices are stores in the map as keys and a collection of adjacent vertices as a value for given key.
 * users must follow equals() and getHash() contract for vertices otherwise the behavior is unpredictable and method contracts are not guaranteed.
 **/
public abstract class AbstractGraph<V, E extends Edge<V>> implements Graph<V, E> {

    protected final Map<V, Collection<V>> vertices;

    protected AbstractGraph(final Map<V, Collection<V>> vertices) {
        this.vertices = vertices;
    }

    protected AbstractGraph() {
        this(new HashMap<>());
    }

    protected abstract E getEdge(V from, V to);

    @Override
    public Collection<E> getEdges(final V v) {
        if (isNull(v)) {
            throw new NullPointerException("vertex can't be null");
        }

        if (!isVertexPresent(v)) {
            return EMPTY_SET;
        }

        return vertices.get(v).stream().map(v1 -> getEdge(v, v1)).collect(toSet());
    }

    @Override
    public boolean hasEdge(final Edge edge) {
        final V sourceVertex = (V) edge.getSourceVertex();
        final V destinationVertex = (V) edge.getDestinationVertex();
        if (!isVertexPresent(sourceVertex) || !isVertexPresent(destinationVertex)) {
            return false;
        }

        return vertices.get(sourceVertex).contains(destinationVertex);
    }

    @Override
    public E addEdge(final V sourceVertex, final V targetVertex) {
        if (isNull(sourceVertex) || isNull(targetVertex)) {
            throw new NullPointerException("vertex can't be null");
        }

        if (!isVertexPresent(sourceVertex) || !isVertexPresent(targetVertex)) {
            throw new GraphException("failed to add edge: vertex must be in graph. sourceVertex" + sourceVertex + " targetVertex" + targetVertex);
        }

        final Collection<V> vs = vertices.get(sourceVertex);
        if (!vs.contains(targetVertex)) {
            vs.add(targetVertex);
        }
        return getEdge(sourceVertex, targetVertex);
    }

    @Override
    public boolean addVertex(final V v) {
        if (isNull(v)) {
            throw new NullPointerException("vertex can't be null");
        }

        if (isVertexPresent(v)) {
            return false;
        }

        vertices.computeIfAbsent(v, getMappingFunction());

        return true;
    }

    @Override
    public List<E> getPath(final V start, final V end) {
        if (isNull(start) || isNull(end)) {
            throw new NullPointerException("failed to get path for null vertex. source : " + start + " , target: " + end);
        }

        if (!isVertexPresent(start) || !isVertexPresent(end)) {
            return EMPTY_LIST;
        }

        final Optional<ParentAwareVertexWrapper> chain = buildChain(start, end);
        if (chain.isPresent()) {
            final LinkedList<E> result = new LinkedList<>();
            ParentAwareVertexWrapper vertex = chain.get();
            while (vertex.parent != null) {
                result.addFirst(getEdge(vertex.parent.v, vertex.v));
                vertex = vertex.parent;
            }

            return result;
        }

        return EMPTY_LIST;
    }

    /**
     * the simplest BTF-like algorithm for searching a vertex in the graph.
     * each vertex receives a 'parent' during the traversal process so it is possible to
     * restore a path from end to start node.
     *
     * @see https://en.wikipedia.org/wiki/Graph_traversal#Breadth-first_search
     * @see https://en.wikipedia.org/wiki/Breadth-first_search
     */
    protected Optional<ParentAwareVertexWrapper> buildChain(final V start, final V end) {
        if (start.equals(end)) {
            return Optional.of(new ParentAwareVertexWrapper(start, new ParentAwareVertexWrapper(end, null)));
        }

        final Set<ParentAwareVertexWrapper> visited = new HashSet<>();
        final LinkedList<ParentAwareVertexWrapper> queue = new LinkedList<>();
        final ParentAwareVertexWrapper startNode = new ParentAwareVertexWrapper(start, null);
        queue.add(startNode);
        visited.add(startNode);

        while (!queue.isEmpty()) {
            final ParentAwareVertexWrapper v = queue.removeFirst();
            for (V child : getAdjustedVertices(v.v)) {
                final ParentAwareVertexWrapper current = new ParentAwareVertexWrapper(child, v);
                if (!visited.contains(current)) {
                    visited.add(current);
                    if (child.equals(end)) {
                        return Optional.of(current);
                    } else {
                        queue.add(current);
                    }
                }
            }
        }

        return empty();
    }


    protected class ParentAwareVertexWrapper {

        public ParentAwareVertexWrapper(final V v, final ParentAwareVertexWrapper parent) {
            this.v = v;
            this.parent = parent;
        }

        V v;
        ParentAwareVertexWrapper parent;

        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            final ParentAwareVertexWrapper that = (ParentAwareVertexWrapper) o;
            return Objects.equals(v, that.v);
        }

        @Override
        public int hashCode() {
            return Objects.hash(v);
        }
    }

    /**
     * return vertices nodes for given vertex. Users can override this method to be able to setup a specific order of vertex processing (eg take weighted edges into consideration)
     */
    protected Iterable<V> getAdjustedVertices(V v) {
        return this.getEdges(v).stream().map(Edge::getDestinationVertex).collect(toSet());
    }

    @Override
    public Collection<V> getVertices() {
        return vertices.keySet();
    }

    protected boolean isVertexPresent(V v) {
        return vertices.containsKey(v);
    }

    protected Function<? super V, ? extends Collection<V>> getMappingFunction() {
        return v -> new LinkedHashSet<>();
    }

    @Override
    public String toString() {
        return "{" +
                "vertices=" + vertices +
                '}';
    }
}
