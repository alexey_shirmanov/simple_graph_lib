package com.natera.simplegraph;

public interface Edge<V> {
    /**
     * @return source vertex of current edge
     * */
    V getSourceVertex();

    /**
     * @return destination vertex of current edge
     * */
    V getDestinationVertex();
}
