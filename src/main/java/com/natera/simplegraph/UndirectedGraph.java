package com.natera.simplegraph;

public class UndirectedGraph<V, E extends Edge<V>> extends AbstractGraph<V, Edge<V>> {

    @Override
    protected Edge<V> getEdge(final V from, final V to) {
        return new SimpleEdge<>(from, to);
    }

    @Override
    public Edge<V> addEdge(final V sourceVertex, final V targetVertex) {
        super.addEdge(targetVertex, sourceVertex);
        return super.addEdge(sourceVertex, targetVertex);
    }
}
