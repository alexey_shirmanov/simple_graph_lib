package com.natera.simplegraph;

import java.util.Collection;
import java.util.List;

public interface Graph<V, E> {

    /**
     * adds an edge between {@code sourceVertex} and {@code targetVertex} into graph
     *
     * @return created edge
     * @throws NullPointerException if either {@code sourceVertex} or {@code targetVertex} is {@code null}
     * @throws GraphException       if either {@code sourceVertex} or {@code targetVertex} is not vertex of current graph
     */
    E addEdge(V sourceVertex, V targetVertex) throws NullPointerException, GraphException;

    /**
     * adds vertex {@code v} into graph
     *
     * @throws NullPointerException if vertex {@code v} is {@code null}
     * @return {@code true} if vertex was added to the graph and {@code false} if graph already contains this vertex
     */
    boolean addVertex(V v) throws NullPointerException;

    /**
     * return a path between two vertices in the graph as list of edges
     *
     * @return an empty list is returned if graph doesn't contain one or both of given vertices.
     * Also the empty list can be returned if there is no path between two vertices.
     * @throws NullPointerException if either {@code from} vertex of {@code to} vertex is {@code null}
     * */
    List<E> getPath(V from , V to)throws NullPointerException;

    /**
     * return graph's vertices
     *
     * @return Collection of vertices or an empty collection if graph is empty
     * */
    Collection<V> getVertices();

    /**
     * checks if graph has given edge
     *
     * @return {@code true} if graph contains given edge, {@code false} otherwise
     * */
    boolean hasEdge(E e);

    /**
     * return collection of outgoing edges for given vertex {@code v}
     *
     * @return a collection of outgoing edges or an empty collection if vertex has no edges.
     * an empty collection is returned if graph doesn't contain given vertex.
     * @throws NullPointerException if given vertex is {@code null}
     * */
    Collection<E> getEdges(V v) throws NullPointerException;
}
