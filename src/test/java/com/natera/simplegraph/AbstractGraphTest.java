package com.natera.simplegraph;

import org.assertj.core.api.Condition;

import java.util.Collection;
import java.util.HashSet;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.assertj.core.util.Lists.newArrayList;

public class AbstractGraphTest {
    protected String givenStringVertex() {
        return randomAlphanumeric(10);
    }

    protected Collection<String> givenStringVertices() {
        return givenStringVertices(10);
    }

    protected Collection<String> givenStringVertices(int count) {
        Collection<String> res = new HashSet<>();//set of unique vertices
        while (res.size() != count) {
            res.add(givenStringVertex());
        }

        return res;
    }

    static class ContainsCondition<E> extends Condition<Iterable<E>> {
        private Collection<E> collection;

        public ContainsCondition(Iterable<E> values) {
            super("contains " + values);
            this.collection = newArrayList(values);
        }

        static<E> ContainsCondition contains(Collection<E> set) {
            return new ContainsCondition(set);
        }

        @Override
        public boolean matches(Iterable<E> actual) {
            Collection<E> values = newArrayList(actual);
            for (E e : collection) {
                if (!values.contains(e)) return false;
            }
            return true;
        }

    }

}
