package com.natera.simplegraph;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DirectedGraphTest extends AbstractGraphTest {
    @Test
    public void getValidEdgesForPresentedVertex() throws Exception {
        Graph<String, Edge<String>> stringGraph = new DirectedGraph<>();
        final String first = givenStringVertex();
        final String second = givenStringVertex();
        stringGraph.addVertex(first);
        stringGraph.addVertex(second);
        stringGraph.addEdge(first, second);

        assertThat(stringGraph.getEdges(first)).containsOnly(new SimpleEdge<>(first, second));
        assertThat(stringGraph.getEdges(second)).doesNotContain(new SimpleEdge<>(second, first));
    }

    @Test
    public void testGetPathForVertices() throws Exception {
        final String one = givenStringVertex();
        final String two = givenStringVertex();
        final String three = givenStringVertex();
        final String four = givenStringVertex();
        final String five = givenStringVertex();
        final String six = givenStringVertex();

        final Graph<String, Edge<String>> stringEdgeGraph = givenDirectedGraph(one, two, three, four, five, six);
        stringEdgeGraph.addEdge(one, two);
        stringEdgeGraph.addEdge(two, three);
        stringEdgeGraph.addEdge(three, six);
        stringEdgeGraph.addEdge(one, four);
        stringEdgeGraph.addEdge(four, five);


        LinkedList<Edge<String>> expectedPath = new LinkedList<Edge<String>>() {{
            addLast(new SimpleEdge<>(one, two));
            addLast(new SimpleEdge<>(two, three));
            addLast(new SimpleEdge<>(three, six));
        }};

        final List<Edge<String>> path = stringEdgeGraph.getPath(one, six);
        assertThat(path).containsExactlyElementsOf(expectedPath);
        assertThat(stringEdgeGraph.getPath(six, one)).isEmpty();
    }

    private Graph<String, Edge<String>> givenDirectedGraph(final String... vert) {
        Graph<String, Edge<String>> stringGraph = new DirectedGraph<>();
        for (final String s : vert) {
            stringGraph.addVertex(s);
        }
        return stringGraph;
    }

}