package com.natera.simplegraph;

import org.junit.Test;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static com.natera.simplegraph.AbstractGraphTest.ContainsCondition.contains;
import static java.util.Collections.unmodifiableCollection;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.assertj.core.api.Assertions.anyOf;
import static org.assertj.core.api.Assertions.assertThat;

public class UndirectedGraphTest extends AbstractGraphTest {
    @Test(expected = NullPointerException.class)
    public void getEdgesForNullVertex() throws Exception {
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        stringGraph.getEdges(null);
    }

    @Test
    public void getEmptyCollectionForNOtPresentedVertex() throws Exception {
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        final String s = givenStringVertex();
        stringGraph.addVertex(s);

        assertThat(stringGraph.getEdges(s + s)).isEmpty();
    }

    @Test
    public void getValidEdgesForPresentedVertex() throws Exception {
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        final String first = givenStringVertex();
        final String second = givenStringVertex();
        stringGraph.addVertex(first);
        stringGraph.addVertex(second);
        stringGraph.addEdge(first, second);

        Edge<String> firstToSecondEdge = new SimpleEdge<>(first, second);
        Edge<String> secondToFirstEdge = new SimpleEdge<>(second, first);

        assertThat(stringGraph.getEdges(first)).containsOnly(firstToSecondEdge);
        assertThat(stringGraph.getEdges(second)).containsOnly(secondToFirstEdge);
    }

    @Test(expected = NullPointerException.class)
    public void addEdgeSourceNull() throws Exception {
        final Graph<String, Edge<String>> undirectedGraph = new UndirectedGraph<>();
        undirectedGraph.addEdge(null, givenStringVertex());
    }

    @Test(expected = NullPointerException.class)
    public void addEdgeDestinationNull() throws Exception {
        final Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        stringGraph.addEdge(givenStringVertex(), null);
    }

    @Test(expected = GraphException.class)
    public void addEdgeSourceNotPresentedVertex() throws Exception {
        final String givenStringVertex = givenStringVertex();
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        stringGraph.addVertex(givenStringVertex);

        stringGraph.addEdge(givenStringVertex.concat(givenStringVertex)/*100% not present*/, givenStringVertex);
    }

    @Test(expected = GraphException.class)
    public void addEdgeDestinationNotPresentedVertex() throws Exception {
        final String givenStringVertex = givenStringVertex();
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        stringGraph.addVertex(givenStringVertex);

        stringGraph.addEdge(givenStringVertex, givenStringVertex.concat(givenStringVertex)/*100% not present*/);
    }

    @Test
    public void addValidEdge() throws Exception {
        final String firstVertex = givenStringVertex();
        final String secondVertex = givenStringVertex();

        Edge<String> firstToSecondEdge = new SimpleEdge<>(firstVertex, secondVertex);
        Edge<String> secondToFirstEdge = new SimpleEdge<>(firstVertex, secondVertex);

        Graph<String, Edge<String>> stringGraph = givenUndirectedGraph(firstVertex, secondVertex);

        final Edge<String> edge = stringGraph.addEdge(firstVertex, secondVertex);
        assertThat(edge).isEqualTo(firstToSecondEdge);
        assertTrue(stringGraph.hasEdge(firstToSecondEdge));
        assertTrue(stringGraph.hasEdge(secondToFirstEdge));
    }

    @Test
    public void addTheSameEdgeTwice() throws Exception {
        final String firstVertex = givenStringVertex();
        final String secondVertex = givenStringVertex();
        Graph<String, Edge<String>> stringGraph = givenUndirectedGraph(firstVertex, secondVertex);

        final Edge<String> edgeOne = stringGraph.addEdge(firstVertex, secondVertex);
        final Edge<String> edgeTwo = stringGraph.addEdge(firstVertex, secondVertex);
        assertThat(edgeOne).isEqualTo(edgeTwo);
        assertThat(stringGraph.getEdges(firstVertex)).hasSize(1).containsOnly(edgeOne);
    }

    private Graph<String, Edge<String>> givenUndirectedGraph(final String... vert) {
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        for (final String s : vert) {
            stringGraph.addVertex(s);
        }
        return stringGraph;
    }

    @Test
    public void testHasNotEdge() throws Exception {
        final String firstVertex = givenStringVertex();
        final String secondVertex = givenStringVertex();
        Edge<String> firstToSecondEdge = new SimpleEdge<>(firstVertex, secondVertex);

        Graph<String, Edge<String>> stringGraph = givenUndirectedGraph(firstVertex, secondVertex);

        assertFalse(stringGraph.hasEdge(firstToSecondEdge));
    }

    @Test
    public void testHasNotEdgeForInvalidVertices() throws Exception {
        assertFalse(((Graph<String, Edge<String>>) new UndirectedGraph<String, Edge<String>>()).
                hasEdge(new SimpleEdge<>(givenStringVertex(), givenStringVertex())));
    }

    @Test
    public void testBidirectionalEdge() throws Exception {
        final String firstVertex = givenStringVertex();
        final String secondVertex = givenStringVertex();
        Edge<String> firstToSecondEdge = new SimpleEdge<>(firstVertex, secondVertex);
        Edge<String> secondToFirstEdge = new SimpleEdge<>(secondVertex, firstVertex);

        Graph<String, Edge<String>> stringGraph = givenUndirectedGraph(firstVertex, secondVertex);
        stringGraph.addEdge(firstVertex, secondVertex);

        assertTrue(stringGraph.hasEdge(firstToSecondEdge));
        assertTrue(stringGraph.hasEdge(secondToFirstEdge));
    }

    @Test
    public void testHasNotEdgeFotInvalidVertex() throws Exception {
        final String firstVertex = givenStringVertex();
        final String secondVertex = givenStringVertex();
        final String thirdVertex = firstVertex + secondVertex;

        Edge<String> thirdToFirstEdge = new SimpleEdge<>(thirdVertex, firstVertex);

        Graph<String, Edge<String>> stringGraph = givenUndirectedGraph(firstVertex, secondVertex);
        stringGraph.addEdge(firstVertex, secondVertex);

        assertFalse(stringGraph.hasEdge(thirdToFirstEdge));
    }

    @Test(expected = NullPointerException.class)
    public void addEdgeNullVertex() throws Exception {
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        stringGraph.addEdge(null, givenStringVertex());
    }

    @Test
    public void addAlreadyPresentedVertex() throws Exception {
        String vertex = givenStringVertex();
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();

        final boolean firstResult = stringGraph.addVertex(vertex);
        final Collection<String> verticesAfterFirstAddCall = unmodifiableCollection(stringGraph.getVertices());
        final boolean secondResult = stringGraph.addVertex(vertex);
        final Collection<String> verticesAfterSecondAddCall = unmodifiableCollection(stringGraph.getVertices());

        assertTrue("addVertex returned false for vertex that isn't yet in the graph ", firstResult);
        assertThat(verticesAfterFirstAddCall).containsExactly(vertex);
        assertFalse("addVertex returned true for vertex that is in the graph ", secondResult);
        assertThat(verticesAfterSecondAddCall).containsExactly(vertex);
    }

    @Test(expected = NullPointerException.class)
    public void addNullVertex() throws Exception {
        new UndirectedGraph<String, Edge<String>>().addVertex(null);
    }

    @Test
    public void addVertex() throws Exception {
        final Collection<String> stringvertices = this.givenStringVertices();
        Graph<String, Edge<String>> stringGraph = new UndirectedGraph<>();
        stringvertices.forEach(stringGraph::addVertex);

        assertThat(stringGraph.getVertices()).containsOnlyElementsOf(stringvertices);
    }

    @Test(expected = NullPointerException.class)
    public void testGetPAthForNullvertices() throws Exception {
        final Graph<String, Edge<String>> stringEdgeGraph = givenUndirectedGraph(givenStringVertex(), givenStringVertex());
        stringEdgeGraph.getPath(null, null);
    }

    @Test
    public void testGetPathForInvalidVertices() throws Exception {
        final String firstVertex = givenStringVertex();
        final String secondVertex = givenStringVertex();
        final Graph<String, Edge<String>> stringEdgeGraph = givenUndirectedGraph(firstVertex, secondVertex);
        final List<Edge<String>> path = stringEdgeGraph.getPath(firstVertex + secondVertex, secondVertex + firstVertex);
        assertThat(path).isEmpty();
    }

    @Test
    public void testGetPathForTheSameVertex() throws Exception {
        final String vertex = givenStringVertex();
        final Graph<String, Edge<String>> stringEdgeGraph = givenUndirectedGraph(vertex);

        final List<Edge<String>> path = stringEdgeGraph.getPath(vertex, vertex);
        assertThat(path).containsOnly(new SimpleEdge<>(vertex, vertex));
    }

    @Test
    public void testGetPathForNonConnectedVertices() throws Exception {
        final String vertexOne = givenStringVertex();
        final String vertexTwo = givenStringVertex();

        assertThat(givenUndirectedGraph(vertexOne, vertexTwo).getPath(vertexOne, vertexTwo)).isEmpty();
    }

    @Test
    /**
     * undirected test graph like following
     *
     * one<-->two<-->three<-->six
     *  ^
     *  |
     *  \--->four<-->five
     *
     * */
    public void testGetPathForVertices() throws Exception {
        final String one = givenStringVertex();
        final String two = givenStringVertex();
        final String three = givenStringVertex();
        final String four = givenStringVertex();
        final String five = givenStringVertex();
        final String six = givenStringVertex();

        final Graph<String, Edge<String>> stringEdgeGraph = givenUndirectedGraph(one, two, three, four, five, six);
        stringEdgeGraph.addEdge(one, two);
        stringEdgeGraph.addEdge(two, three);
        stringEdgeGraph.addEdge(three, six);
        stringEdgeGraph.addEdge(one, four);
        stringEdgeGraph.addEdge(four, five);


        LinkedList<Edge<String>> expectedPath = new LinkedList<Edge<String>>() {{
            addLast(new SimpleEdge<>(one, two));
            addLast(new SimpleEdge<>(two, three));
            addLast(new SimpleEdge<>(three, six));
        }};

        LinkedList<Edge<String>> reversedPath = new LinkedList<Edge<String>>() {{
            addLast(new SimpleEdge<>(six, three));
            addLast(new SimpleEdge<>(three, two));
            addLast(new SimpleEdge<>(two, one));
        }};

        final List<Edge<String>> path = stringEdgeGraph.getPath(one, six);
        assertThat(path).containsExactlyElementsOf(expectedPath);
        final List<Edge<String>> pathBack = stringEdgeGraph.getPath(six, one);
        assertThat(pathBack).containsExactlyElementsOf(reversedPath);
    }

    @Test
    public void testGetPathForCircledGraph() throws Exception {
        final String one = givenStringVertex();
        final String two = givenStringVertex();
        final String three = givenStringVertex();
        final String four = givenStringVertex();

        final Graph<String, Edge<String>> stringEdgeGraph = givenUndirectedGraph(one, two, three, four);
        stringEdgeGraph.addEdge(one, two);
        stringEdgeGraph.addEdge(two, three);
        stringEdgeGraph.addEdge(three, four);
        stringEdgeGraph.addEdge(four, one);


        LinkedList<Edge<String>> firstPossiblePath = new LinkedList<Edge<String>>() {{
            addLast(new SimpleEdge<>(one, two));
            addLast(new SimpleEdge<>(two, three));
        }};
        LinkedList<Edge<String>> secondPossiblePath = new LinkedList<Edge<String>>() {{
            addLast(new SimpleEdge<>(one, four));
            addLast(new SimpleEdge<>(four, three));
        }};

        final List<Edge<String>> path = stringEdgeGraph.getPath(one, three);
        assertThat(path).has(anyOf(contains(firstPossiblePath), contains(secondPossiblePath)));

    }

}